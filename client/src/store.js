import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    year: 2020
  },
  mutations: {
    updateYear(state, year) {
      state.year = year
    }
  }
})