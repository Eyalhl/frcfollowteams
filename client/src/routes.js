import FavoritesPage from '@/views/FavoritesPage'
import MainPage from '@/views/MainPage'

const routes = [
    {path: '/', component: MainPage},
    {path: '/favorites', component: FavoritesPage},
];

export default routes;